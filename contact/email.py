from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.utils.text import wrap


def send_html_email(subject, from_email, to, template, data, text_only=False,
                    reply_to=None, send_separately=False):
    """ Send HTML email based on a template """

    # Helper func to actually perform the sending
    def send_message(msg, html):
        if not text_only:
            # Attach the HTML part as needed
            msg.attach_alternative(html, "text/html")
        msg.send()

    # ``to`` must be a list
    if not type(to) is list:
        to = [to]

    if text_only:
        # Consider the template a plain-text template and set HTML to None
        html = None
        text = render_to_string(template, data)
    else:
        # Render the HTML template and create the text version by stripping
        # HTML tags from the resulting HTML
        html = render_to_string(template, data)
        text = wrap(strip_tags(html), 75)

    # Extra headers (none by default)
    headers = {}

    # Set up Reply-To header if necessary
    if reply_to:
        headers['Reply-To'] = reply_to

    if to.__iter__ and len(to) and send_separately:
        # We need to send to each recipient a separate copy of the message.
        for r in to:
            send_message(
                EmailMultiAlternatives(subject, text, from_email, [r],
                                       headers=headers),
                html
            )
    else:
        # Send normally (note that when sending to multiple recipients, each
        # recipient will be able to see the other recipients' emails. Use
        # ``send_separately`` argument to send multiple messages.
        send_message(
            EmailMultiAlternatives(subject, text, from_email, to,
                                   headers=headers),
            html
        )

